<?php

require_once 'common.php';

const BASE_URL = 'http://localhost:8080/';

class Hw4Tests extends HwTests {

    function baseUrlResponds() {
        $this->get(BASE_URL);
        $this->assertResponse(200);
    }

    function listPageHasMenuWithCorrectLinks() {
        $this->get(BASE_URL);

        $this->assertLinkById('list-page-link');
        $this->assertLinkById('add-page-link');

    }

    function addPageHasCorrectElements() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->assertField('firstName');
        $this->assertField('lastName');
        $this->assertField('phone');

        $this->assertField('submitButton');
    }

    function submittingFormAddsPersonToList() {

        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $person = getSampleData();

        $this->setFieldByName('firstName', $person->firstName);
        $this->setFieldByName('lastName', $person->lastName);
        $this->setFieldByName('phone', $person->phone);

        $this->clickSubmitByName('submitButton');

        $this->assertText($person->firstName);
        $this->assertText($person->lastName);
        $this->assertText($person->phone);
    }

    function canHandleDifferentSymbols() {

        $this->get(BASE_URL);
        $this->clickLinkById('add-page-link');

        $name = "!\"'#%.,:;\n" . getSampleData()->firstName;

        $this->setFieldByName('firstName', $name);

        $this->clickSubmitByName('submitButton');

        $escapedName = htmlspecialchars($name, ENT_QUOTES);

        $pattern = sprintf("/(%s)|(%s)/",
            preg_quote($name),
            preg_quote($escapedName));

        $this->assertPattern($pattern);
    }

}

(new Hw4Tests())->run(new PointsReporter());
